package com.pete.week8;

public class Map2 {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int width_MIN2 = 0;
    public final static int width_MAX2 = 9;
    public final static int height_MIN2 = 0;
    public final static int height_MAX2 = 9;

    public Map2(String name, char symbol, int x, int y){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;

    }

    public Map2(String name, char symbol){
        this(name, symbol, 0, 0);
    }

    public char getSymbol(){
        return symbol;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public static void println() {
    }
}
