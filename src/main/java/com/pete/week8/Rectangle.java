package com.pete.week8;

public class Rectangle {
    private double height;
    private double width;

    public Rectangle(double width , double height){
        this.width = width;
        this.height = height;
    }

    public double getArea(){
        return width * height;
    }

    public double getPerimeter(){
        return 2*(width+height);
    }
}
