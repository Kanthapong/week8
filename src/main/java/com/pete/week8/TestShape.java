package com.pete.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        System.out.println(rect1.getArea());
        System.out.println(rect1.getPerimeter());


        Rectangle rect2 = new Rectangle(5, 3);
        System.out.println(rect2.getArea());
        System.out.println(rect2.getPerimeter());


        Circle circle1 = new Circle(1);
        System.out.println(circle1.getArea());
        System.out.println(circle1.getPerimeter());


        Circle circle2 = new Circle(2);
        System.out.println(circle2.getArea());
        System.out.println(circle2.getPerimeter());


        Triangle triangle = new Triangle(5, 5, 6);
        System.out.println(triangle.getArea());
        System.out.println(triangle.getPerimeter());
    }
}
